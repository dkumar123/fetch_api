import React, { useEffect, useState } from "react";

function FetchApi() {
    const [data, setData] = useState(null);
  const apiGet = () => 
     fetch('http://instapreps.com/api/country_code')
        .then((response) => response.json())

  useEffect(() =>{
    apiGet().then((response) => setData(response.data))
  },[]);
    return(
    <div>
        <div>
            <table className= 'table'>
                <tr>
                    <th className= 'th'>ID</th>
                    <th className= 'th'>Code</th>
                    <th className= 'th'>Country</th>
                </tr>
                {
                data?.map((item,index) => (
                   <tr>
                    <td key={index}><b>
                        {item.id}</b></td>
                    <td key={index}><b>
                        {item.code}</b></td>
                        <td key={index}><b>
                        {item.country}</b></td>
                    </tr>
                ))}

            </table>
        </div>
    </div>
    );       
}

export default FetchApi;